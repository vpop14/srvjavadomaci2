package Arduino;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

//import Arduino.arduinotest1;

import javax.swing.*;

public class TestClass implements MouseListener
{
    JPanel panel;
    public TestClass(){

        panel = new JPanel();
        JFrame frame = new JFrame();
        frame.setSize(400,400);

        panel.addMouseListener(this);
        frame.add(panel);
        frame.setVisible(true);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
    public static BufferedReader input;
    public static OutputStream output;
    public static synchronized void writeData(String data) {
        System.out.println("Sent: " + data);
        try {
            output.write(data.getBytes());
        } catch (Exception e) {
            System.out.println("could not write to port");
        }
    }
public static void main(String[] args) throws IOException {

        arduinotest1 obj = new arduinotest1();
        obj.initialize();

        input = arduinotest1.input;
        output = arduinotest1.output;
        String inputLine = input.readLine();
        if(inputLine.contains("moze")) {
            TestClass t = new TestClass();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

        if(e.getSource() == panel){
            if(e.getButton() == 1){
                writeData("1");

            }
            else if(e.getButton() == 3){
                writeData("2");
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
